package com.infotech.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootLiquibaseIntegration {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootLiquibaseIntegration.class, args);
	}

}
